from __future__ import unicode_literals  # support both Python2 and 3

from mock import patch
import unittest2 as unittest

import json

from pycastiphone_client.client import Client
from pycastiphone_client import exceptions


class FakeResponse:
    status_code = None
    content = ""
    reason = ""

    def __init__(self, status=200, reason="", content="{}"):
        self.status_code = status
        self.content = content
        self.reason = reason

    def json(self):
        return json.loads(self.content)


class ClientTests(unittest.TestCase):
    expected_headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': None
    }
    sample_body = {"a": 1}
    sample_route = "/path"

    @patch("pycastiphone_client.client.requests.request", side_effect=Exception)
    def test_network_error_raises_expected_exception(self, _):
        with self.assertRaises(exceptions.HTTPError):
            Client().post(self.sample_route, self.sample_body)

    @patch(
        "pycastiphone_client.client.requests.request",
        return_value=FakeResponse(status=500),
    )
    def test_server_error_500_raises_expected_exception(self, request_mock):
        request_mock.return_value = FakeResponse(status=500)
        with self.assertRaises(exceptions.HTTPError):
            Client().post(self.sample_route, self.sample_body)

    @patch(
        "pycastiphone_client.client.requests.request",
         return_value=FakeResponse(status=202)
    )
    def test_exception_response_status_code_not_200_or_201(self, request_mock):
         request_mock.return_value = FakeResponse(status=202)
         with self.assertRaises(exceptions.NotSuccessfulRequest):
            Client().post(self.sample_route, self.sample_body)

    @patch(
        "pycastiphone_client.client.requests.request",
        return_value=FakeResponse(),
    )
    def test_put(self, mock_request):

        Client().put(self.sample_route, self.sample_body)

        mock_request.assert_called_once_with(
            "PUT",
            "https://bezeroak.onaro.eus/api/api/path",
            data=json.dumps(self.sample_body),
            headers=self.expected_headers,
        )
